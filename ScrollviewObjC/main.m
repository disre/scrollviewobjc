//
//  main.m
//  ScrollviewObjC
//
//  Created by Kelson Vella on 8/30/17.
//  Copyright © 2017 Kelson Vella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
