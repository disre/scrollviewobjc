//
//  AppDelegate.h
//  ScrollviewObjC
//
//  Created by Kelson Vella on 8/30/17.
//  Copyright © 2017 Kelson Vella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

